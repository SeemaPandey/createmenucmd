import XCTest

import CMTTests

var tests = [XCTestCaseEntry]()
tests += CMTTests.allTests()
XCTMain(tests)