//
//  CreateMenuViewController.swift
//  MngApp
//
//  Created by Seema Pandey on 7/29/16.
//  Copyright © 2016 WirelesslyLinked, LLC. All rights reserved.
//

import Foundation
import Alamofire

class CreateMenus {

    /*
    static func loadSingleMenu(menu: [String: Any]) -> GroupDetail{
        
        BusinessUser.locationId = menu["locId"] as! String
        
        let group = GroupDetail(json: menu)
        
        //print group
        printGroup(group: group)
        
        return group
    }
    */
    
    
    //create location
    static func createLocation(location:LocationDetail, createLocCompleted: @escaping (String?) -> Void){
        
        CloudRequests.createLocation( location.getJson(), completionHandler: { (response:[String : AnyObject]?, error:String?) in
            
            if error != nil {
                
                createLocCompleted(error)
                return
            }
            
            print(location.locName + ": location Updated")
            createLocCompleted(nil)
        })
        
    }
    
    //update Business
    static func updateBusiness(biz:BusinessDetail, updateBizCompleted: @escaping (String?) -> Void){
        
        CloudRequests.updateBusinessDetail(biz.bizId!, param: biz.getJson(), completionHandler: { (response:[String : Any]?, error:String?) in
            
            if error != nil {
                
                updateBizCompleted(error)
                return
            }
            
            print(biz.bizId! + ": business updated.")
            updateBizCompleted(nil)
        })
        
    }
    
    
    //update location
    static func updateLocation(location:LocationDetail, updateLocCompleted: @escaping (String?) -> Void){
        
        CloudRequests.updateLocation(location.getJson(), completionHandler: { (response:[String : AnyObject]?, error:String?) in
            
            if error != nil {
                
                updateLocCompleted(error)
                return
            }
            
            print(location.objectId! + ": location Updated")
            updateLocCompleted(nil)
        })
        
    }
    
    //update menu of location
    
    //proess all items of a group
    static func updateItemsOf(group: GroupDetail, updateItemsCompleted: @escaping (String?) -> Void){
        
        for item in group.items {
            
            if item.isUpdated {
                
                continue
            }
            
            print("updating item. \(group.groupName) -> \(item.itemName) itemId: \(item.itemId ?? "nil") groupId: \(group.objectId)")
            
            
            updateItem(item: item, updateItemCompleted: { (error: String?) in
                
                if error != nil {
                    
                    print("Error: Updating item: \(item) error: \(error!)")
                    updateItemsCompleted(error)
                    return
                }
                
                item.isUpdated = true
                
                self.updateItemsOf(group: group, updateItemsCompleted: updateItemsCompleted)
            })
            
            //return to process only 1 element at a time
            return
        }
        
        updateItemsCompleted(nil)
    }
    
    
    //processGroup
    static func processGroup(group: GroupDetail, operationCompleted: @escaping (String?) -> Void){
        
        if group.isProcessed {
            
            operationCompleted(nil)
            return
        }
        
        group.isProcessed = true
        
        print("processing group name: \(group.groupName!) id: \(group.objectId) groupType: \(group.groupType) parentGroup: \(group.parentGroup?.groupName ?? "nil")")
        
        //1. Update this group first
        self.updateGroupDetail(groupDetail: group, updateGroupCompleted: { (error: String?) in
            
            if error != nil {
                
                operationCompleted(error)
                return
            }
            
            //2. process childGroups
            self.processChildGroupsOf(group: group, processChildCompleted: { (processChildGroupsError: String?) in
                
                if processChildGroupsError != nil {
                    
                    operationCompleted(processChildGroupsError)
                    return
                }
                
                //3. process this group items
                self.updateItemsOf(group: group, updateItemsCompleted: { (processItemsError: String?) in
                    
                    if processItemsError != nil {
                        
                        operationCompleted(processItemsError)
                        return
                    }
                    
                    // update childGroupIds
                    group.populateChildGroupIds()
                    
                    //update group itemsIds
                    group.populateItemsIds()
                    
                    //4. Update this group with childGroupIds and itemIds
                    self.updateGroupDetail(groupDetail: group, updateGroupCompleted: { (updateGroupError: String?) in
                        
                        if updateGroupError != nil {
                            
                            operationCompleted(updateGroupError)
                            return
                        }
                        
                        operationCompleted(nil)
                    })
                    
                })
                
            })
            
        })
    }
    
    // process child groups of given group
    static func processChildGroupsOf(group: GroupDetail, processChildCompleted: @escaping (String?) -> Void){
        
        for childGroup in group.childGroups {
            
            if childGroup.isProcessed {
                continue
            }
            
            print("processing child group id: \(childGroup.objectId) groupName: \(childGroup.groupName) groupType: \(childGroup.groupType) parentGroupId: \(group.objectId) parentGroupName: \(group.groupName) parentGroupType: \(group.groupType)")
            
            processGroup(group: childGroup, operationCompleted: { (error: String?) in
                
                if error != nil {
                    
                    processChildCompleted(error)
                    return
                }
                
                self.processChildGroupsOf(group: group, processChildCompleted: processChildCompleted)
            })
            
            //return to process only 1 element at a time
            return
        }
        
        processChildCompleted(nil)
    }
    
    //update GroupDetail in cloud
    static func updateGroupDetail(groupDetail:GroupDetail, updateGroupCompleted: @escaping(String?) -> Void){
        
        Thread.sleep(forTimeInterval: TimeInterval(abs(0.5)))
        
        var params = Parameters()
        
        let isNew = groupDetail.objectId == nil
        
        if !isNew {
            
            params["objectId"] = groupDetail.objectId
        }
        
       
        if !config.isMenuSameForAllLocation{
            guard let userLocation = user.location else{
                print("CreateMenu: location id  is not present")
                exit(1)
            }
            params["locId"] = getPointerFor(.LocationDetail, objectId: userLocation.objectId!)
        }
        
        guard let userBusiness = user.business else{
            print("CreateMenu: business is not set")
            exit(1)
        }
        params["bizId"] = getPointerFor(.BusinessDetail, objectId: userBusiness.objectId!)
        
        params["groupName"] = groupDetail.groupName
        
        if groupDetail.itemIds.count > 0 {
            params["itemIds"] = groupDetail.itemIds
        }
        
        if let openHours = groupDetail.openHours {
            params["openHours"] = openHours
        }
        
        params["groupType"] = groupDetail.groupType
        
        if let parentGroup = groupDetail.parentGroup {
            
            params["parentGroup"] = getPointerFor(.GroupDetail, objectId: parentGroup.objectId)
        }
        
        if groupDetail.childGroupIds.count > 0 {
            
            params["childGroupIds"] = groupDetail.childGroupIds
        }
        
        //print("updateGroupDetail isNew: \(isNew) params: \(params)")
        
        CloudRequests.updateGroupDetail(isNew, params: params, completionHandler: { (response:[String : AnyObject]?, error:String?) in
            
            if error != nil{
                
                updateGroupCompleted("Group can not be updated because " + error!)
                return
            }
            
            if isNew {
                
                let jsonData = response!["data"] as! [String: AnyObject]
                
                groupDetail.objectId = jsonData["objectId"] as! String
                
                print("\n** \(groupDetail.groupName!) New Group Created. id: \(groupDetail.objectId!) groupName: \(groupDetail.groupName!) parentGroup: \(groupDetail.parentGroup?.objectId ?? "nil")")
                
            }
            else{
                
                print("\n** \(groupDetail.groupName!)  group updated. id: \(groupDetail.objectId!) groupName: \(groupDetail.groupName!) parentGroup: \(groupDetail.parentGroup?.objectId ?? "nil")")
            }
            
            updateGroupCompleted(nil)
        })
    }
    
    
    //update ItemDetail in cloud
    static func updateItem(item:ItemDetail, updateItemCompleted: @escaping(String?) -> Void){
        
        Thread.sleep(forTimeInterval: TimeInterval(abs(0.5)))
        
        var params = Parameters()
        
        let isNew = item.itemId == nil
        
        if !isNew {
            
            params["objectId"] = item.itemId
            
            if item.delete {
                
                sendDeleteItem(item: item, itemDeleted: { (deleteItemError: String?) in
                    
                    //NOTE: if there is any error in deleting item - we just print and don't pass
                    // it up the chain as we don't want to stop if item id doesn't exist
                    if deleteItemError != nil {
                        
                        print("ERROR: delete item failed. itemId: " + item.itemId! + " itemName: " + item.itemName)
                    }
                    
                    updateItemCompleted(nil)
                })
                return
            }
        }
        
        params["itemName"] = item.itemName
        params["itemPrice"] = item.itemPrice
        params["itemDescription"] = item.itemDescription
        params["live"] = true
        
        params["groupDetailPointer"] = getPointerFor(.GroupDetail, objectId: item.groupDetail!.objectId)

        if !config.isMenuSameForAllLocation{
            guard let userLocation = user.location else{
                print("CreateMenu: location id  is not present")
                exit(1)
            }
            params["locId"] = getPointerFor(.LocationDetail, objectId: userLocation.objectId!)
        }

        guard let userBusiness = user.business else{
            print("CreateMenu: business id is not present")
            exit(1)
        }
        params["bizId"] = getPointerFor(.BusinessDetail, objectId: userBusiness.objectId!)
        
        if let opt = item.option{
            params["optionCategories"] = opt
            
            // check the item and option category is duplicate or not
           /*let duplicateOptionCategory = checkDuplicateOptionCategoryIsExist(optionCategories: opt, itemName: item.itemName)
            if duplicateOptionCategory == true {
                // if true then there is duplicates value exists
                print(duplicateOptionCategory)
                return
            }*/
        }
        
        if let mediaArray = item.mediaArray {
            params["mediaArray"] = mediaArray
        }
        
        //print("updateItem isNew: \(isNew) params: \(params)")
        
        
        CloudRequests.updateItemDetail(isNew, params: params, completionHandler: { (response:[String : AnyObject]?, error:String?) in
            
            if error != nil {
                
                updateItemCompleted(error)
                return
            }
            
            if isNew {
                
                let data = response!["data"] as! [String: AnyObject]
                
                item.itemId = (data["objectId"] as! String)
                
                //                print( "\n" + item.itemName + " item created. itemId: " + item.itemId! + " groupName: " + item.groupDetail!.groupName + " groupId: " + item.groupDetail!.objectId )
            }
            else {
                
                //                print( "\n" +  item.itemName + " item updated. itemId: " + item.itemId! + " groupName: " + item.groupDetail!.groupName + " groupId: " + item.groupDetail!.objectId )
            }
            
            updateItemCompleted(nil)
        })
    }
    
    //update groupIds of location
    
    /*
     static func updateGroupIdsOf(location:LocationDetail, updateGroupIdsCompleted: @escaping(String?) -> Void){
     
     var param = Parameters()
     
     param["objectId"] = location.objectId
     param["groupIds"] = location.groupIds
     
     CloudRequests.updateLocation(param) { (response, error) in
     
     if error != nil {
     
     updateGroupIdsCompleted(error)
     return
     }
     
     updateGroupIdsCompleted(nil)
     }
     }
     */
    
    static func updateGroupIdsOf(business:BusinessDetail, updateGroupIdsCompleted: @escaping(String?) -> Void){
        
        var param = Parameters()
        
        param["objectId"] = business.bizId!
        param["groupIds"] = business.groupIds
        
        CloudRequests.updateLocation(param) { (response, error) in
            
            if error != nil {
                
                updateGroupIdsCompleted(error)
                return
            }
            
            updateGroupIdsCompleted(nil)
        }
    }
    
    
    //
    // MARK: delete group
    //
    
    //deleteGroup
    static func deleteGroup(group: GroupDetail, operationCompleted: @escaping (String?) -> Void){
        
        if group.items.isEmpty && group.childGroups.isEmpty {
            
            sendGroupDelete(group: group, asyncResponse: { (error: String?) in
                
                operationCompleted(error)
            })
            
            return
        }
        
        // if this group has child group - attempt to delete last group in childGroups
        if !group.childGroups.isEmpty {
            
            let childGroup = group.childGroups.last!
            
            
            // delete items of childGroup
            deleteItemsOfGroup(group: childGroup) { (error: String?) in
                
                if error != nil {
                    
                    operationCompleted(error)
                    return
                }
                
                sendGroupDelete(group: childGroup, asyncResponse: { (groupDeleteError: String?) in
                    
                    if groupDeleteError != nil {
                        
                        operationCompleted(groupDeleteError)
                        return
                    }
                    
                    group.childGroups.removeLast()
                    
                    //recursive call
                    deleteGroup(group: group, operationCompleted: operationCompleted)
                })
                
            }
        }
        
        // delete items of group
        deleteItemsOfGroup(group: group) { (error: String?) in
            
            if error != nil {
                
                operationCompleted(error)
                return
            }
            
            sendGroupDelete(group: group, asyncResponse: { (groupDeleteError: String?) in
                
                operationCompleted(groupDeleteError)
                
            })
            
        }
        
    }
    
    
    // delete child groups
    private static func deleteChildGroupsOf(group: GroupDetail, asyncResponse: @escaping (String?) -> Void){
        
        if group.childGroups.isEmpty {
            
            asyncResponse(nil)
            return
        }
        
        sendGroupDelete(group: group.childGroups.last!) { (error: String?) in
            
            if error != nil {
                
                print("ERROR: sendGroupDelete: \(error)" )
                return
            }
            
            group.childGroups.removeLast()
            
            deleteChildGroupsOf(group: group, asyncResponse: asyncResponse)
        }
        
    }
    
    
    // delete items of the given group
    private static func deleteItemsOfGroup(group: GroupDetail, itemsDeleted: @escaping (String?) -> Void){
        
        if group.items.isEmpty {
            
            itemsDeleted(nil)
            return
        }
        
        sendDeleteItem(item: group.items.last!) { (itemDeleteError: String?) in
            
            if itemDeleteError != nil {
                
                print("ERROR: itemDeleteError: \(itemDeleteError)" )
                return
            }
            
            group.items.removeLast()
            
            deleteItemsOfGroup(group: group, itemsDeleted: itemsDeleted)
        }
        
    }
    
    //delete single item
    private static func sendDeleteItem(item: ItemDetail, itemDeleted: @escaping (String?) -> Void) {
        
        Thread.sleep(forTimeInterval: TimeInterval(abs(0.5)))
        
        var params = [String: AnyObject] ()
        
        params["objectId"] = item.itemId as AnyObject?
        
        guard let userBusiness = user.business else{
            print("CreateMenu: business id  is not present")
            exit(1)
        }
        params["bizId"] = getPointerFor(.BusinessDetail, objectId: userBusiness.objectId!) as AnyObject?
        params["groupDetailPointer"] = getPointerFor(.GroupDetail, objectId: item.groupDetail!.objectId) as AnyObject?
        
        CloudRequests.deleteItemDetail(params, completionHandler: { (response: AnyObject?, error: String?)  in
            
            if error != nil {
                
                itemDeleted(error)
                return
            }
            
            print(item.itemName + " item deleted. itemId: " + item.itemId! + " groupName: " + item.groupDetail!.groupName)
            
            itemDeleted(nil)
        })
    }
    
    
    //delete group
    private static func sendGroupDelete(group: GroupDetail, asyncResponse: @escaping (String?) -> Void) {
        
        Thread.sleep(forTimeInterval: TimeInterval(abs(0.5)))
        
        if !group.items.isEmpty {
            
            asyncResponse("Group: \(String(describing: group.groupName)) NOT empty. Has \(group.items.count) items.")
            return
        }
        
        if !group.childGroups.isEmpty{
            
            asyncResponse("Group: \(String(describing: group.groupName))  NOT empty. Has \(group.childGroups.count) childGroups.")
            return
        }
        
        var params = [String: AnyObject] ()
        
        params["objectId"] = group.objectId as AnyObject?
        
        guard let userBusiness = user.business else{
            print("CreateMenu: business id  is not present")
            exit(1)
        }
        
        params["bizId"] = getPointerFor(.BusinessDetail, objectId: userBusiness.objectId!) as AnyObject?
        
        CloudRequests.deleteGroupDetail(params, completionHandler: { (response: AnyObject?, error: String?)  in
            
            if error != nil {
                
                asyncResponse(error)
                return
            }
            
            print("\(group.groupName!) group deleted. groupId: \(group.objectId!) childGroupsCount: \(group.childGroups.count)")
            
            asyncResponse(nil)
        })
    }
    
    
    
}

