//
//  Media.swift
//  CreateMenu
//
//  Created by Seema Pandey on 11/7/16.
//  Copyright © 2016 Wirelesslylinked. All rights reserved.
//

import Foundation

class Media
{
    var mediaName: String?
    var mediaUrl: URL?

    init(){
        self.mediaName = ""
        self.mediaUrl = URL(string: "")
    }

    convenience init(json:[String:AnyObject]){

        self.init()

        self.mediaUrl = URL(string: (json["url"] as? String)!)
        self.mediaName = json["name"] as? String

    }
}
