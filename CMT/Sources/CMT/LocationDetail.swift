//
//  LocationDetail.swift
//  CreateMenu
//
//  Created by Seema Pandey on 10/18/16.
//  Copyright © 2016 Wirelesslylinked. All rights reserved.
//

import Foundation

class LocationDetail : Hashable, CustomStringConvertible {
    
    // Initializing values so that when first time json file will be created these value will be present
    var objectId: String?
    
    var bizId: String?
    
    var locName: String!
    
    var desc: String?
    
    var lbName = ""
    
    var location = [String:Any]()
    
    var estimatedDeliveryTime: String?
    
    var estimatedPickUpTime: String?
    
    var phoneNumber: String?
    var url: String?
    var addLine1: String?
    var addLine2: String?
    var zipCode: String?
    var city: String?
    var state: String?
    var country = "USA"
    
    var logoURL: String?
    
    var mediaArray: [[String: Any]]?
    
    var taxes = 0.0
    
    var openHours = [String:String]()
    
    var locationConfig = [String:Any]()
    
    var pvtConfig = [String:Any]()
    
    var appConfig = [String:Any]()
    
    var deliveryConfig = [String:Any]()
    
    var deliveryInfo = [String:Any]()
    
    var timeZone = ""
    
    var orderAcceptTime = 5
    
    
    // These fields are present in cloud, but as they are not used anywhere in our system so ignoring these.
    var templateOvd:String?
    var activetemplate:Any?
    var webTemplate:String?
    
    var pickUpHours:[String:String]?
    var deliveryHours:[String:String]?
    
    init(id:String){
        self.objectId = id
    }
    
    init( )
    {
        //self.locName = name
    }
    
    func set(json: [String:Any]){
        
        self.objectId = json["objectId"] as? String
        
        self.bizId = json["bizId"] as? String
        
        if let lName = json["name"] as? String {
            self.locName = lName
        }
        
        self.desc = json["desc"] as? String
        
        if let lbName = json["lbName"] as? String{
            self.lbName = lbName
        }
        
        if let location = json["location"] as? [String : AnyObject]{
            self.location = location
        }
        
        if let estimatedDeliveryTime = json["estimatedDeliveryTime"] as? String{
            self.estimatedDeliveryTime = estimatedDeliveryTime
        }
        
        if let estimatedPickUpTime = json["estimatedPickUpTime"] as? String{
            self.estimatedPickUpTime = estimatedPickUpTime
        }
        
        if let phoneNumber = json["phoneNumber"] as? String{
            self.phoneNumber = phoneNumber
        }
        
        if let url = json["url"] as? String{
            self.url = url
        }
        
        if let addLine1 = json["addLine1"] as? String{
            self.addLine1 = addLine1
        }
        
        if let addLine2 = json["addLine2"] as? String{
            self.addLine2 = addLine2
        }
        
        if let zipCode = json["zipCode"] as? String{
            self.zipCode = zipCode
        }
        
        if let city = json["city"] as? String{
            self.city = city
        }
        
        if let state = json["state"] as? String{
            self.state = state
        }
        
        if let country = json["country"] as? String{
            self.country = country
        }
        
        if let logoURL = json["logoURL"] as? String{
            self.logoURL = logoURL
        }
        
        if let mediaArray = json["mediaArray"] as? [[String : AnyObject]]{
            self.mediaArray = mediaArray
        }
        
        if let taxes = json["taxes"] as? Double{
            self.taxes = taxes
        }
        
        
        if let openHours = json["openHours"] as? [String : String]{
            self.openHours = openHours
        }
        
        
        if let appConfig = json["appConfig"] as? [String : AnyObject]{
            self.appConfig = appConfig
        }
        
        if let locationConfig = json["locationConfig"] as? [String : AnyObject]{
            self.locationConfig = locationConfig
        }
        
        if let pvtConfig = json["pvtConfig"] as? [String : AnyObject]{
            if !pvtConfig.isEmpty {
                self.pvtConfig = pvtConfig
            }
        }
        
        if let deliveryConfig = json["deliveryConfig"] as? [String : AnyObject]{
            if !deliveryConfig.isEmpty {
                self.deliveryConfig = deliveryConfig
            }
        }
        
        if let deliveryInfo = json["deliveryInfo"] as? [String : AnyObject]{
            if !deliveryInfo.isEmpty {
                self.deliveryInfo = deliveryInfo
            }
        }
        
        if let timeZone = json["timeZone"] as? String{
            self.timeZone = timeZone
        }
        
        if let orderAcceptTime = json["orderAcceptTime"] as? Int{
            self.orderAcceptTime = orderAcceptTime
        }
        
        if let pickupHours = json["pickUpHours"] as? [String:String]{
            self.pickUpHours = pickupHours
        }
        if let delvryHours = json["deliveryHours"] as? [String:String] {
            self.deliveryHours = delvryHours
        }
    }
    
    func getJson() -> [String:Any]{
        
        var json = [String:Any]()
        
        if config.saveObjectId && objectId != nil {
            json["objectId"] = objectId
        }
        
        if bizId != nil {
            json["bizId"] = bizId
        }
        
        json["name"] = locName
        json["desc"] = desc
        json["lbName"] = lbName
        
        json["location"] = location
        
        if estimatedPickUpTime != nil {
            json["estimatedPickUpTime"] = estimatedPickUpTime
        }
        
        if estimatedDeliveryTime != nil {
            json["estimatedDeliveryTime"] = estimatedDeliveryTime
        }
        
        json["phoneNumber"] = phoneNumber
        json["url"] = url
        json["addLine1"] = addLine1
        
        if addLine2 != nil {
            json["addLine2"] = addLine2
        }
        
        json["zipCode"] = zipCode
        json["city"] = city
        json["state"] = state
        json["country"] = country
        
        if logoURL != nil {
            json["logoURL"] = logoURL
        }
        
        if mediaArray != nil {
            json["mediaArray"] = mediaArray
        }
        
        json["taxes"] = taxes
        
        
        json["openHours"] = openHours
        json["appConfig"] = appConfig
        json["locationConfig"] = locationConfig
        
        if !pvtConfig.isEmpty {
            json["pvtConfig"] = pvtConfig
        }
        
        if !deliveryConfig.isEmpty {
            json["deliveryConfig"] = deliveryConfig
        }
        
        if !deliveryInfo.isEmpty {
            json["deliveryInfo"] = deliveryInfo
        }
        
        if let delvHours = self.deliveryHours {
            json["deliveryHours"] = delvHours
        }
        
        /*if let pikupHrs = self.pickUpHours {
            json["pickUpHours"] = pikupHrs
        }*/
        
        json["timeZone"] = timeZone
        
        json["orderAcceptTime"] = orderAcceptTime
        
        /* we no longer store "groups" when saving LocationDetail as json
         if !groupDetailArray.isEmpty{
         
         var groupsJson = [[String:Any]]()
         
         for group in groupDetailArray {
         
         groupsJson.append( group.getJson() )
         }
         
         menuJson["groups"] = groupsJson
         }
         */
        
        return json
    }
    
    
    func downloadLocation(complete: @escaping (String?) -> Void) {
        
        CloudRequests.getLocationDetail(objectId!) { (response, error) in
            
            if error != nil {
                
                complete(error!)
                return
            }
            
            let jsonData = response!["data"] as! [String: AnyObject]
            self.set(json: jsonData)
            
            complete(nil)
        }
    }
    
    
    func writeLocationToFile() throws {
        
        var fileName = self.locName!
        fileName.append("-")
        fileName.append(self.addLine1!)
        
        if config.saveObjectId {
            
            fileName.append("-locId-")
            fileName.append(self.objectId!)
        }
        
        let filePath = getFilePath(fileName: getFileName(name: fileName))
        
        print("Writing to file: " + filePath)
        
        try writeJsonToFile(json: getJson(), filePath: filePath)
    }
    
    
    
    
    var description: String {
        return "objectId: \(objectId ?? "nil") locName: \(locName)"
    }
    
    
    var hashValue: Int {
        get{
            return locName!.hashValue
        }
    }
    
}


func == (lhs: LocationDetail, rhs: LocationDetail) -> Bool {
    return lhs.locName! == rhs.locName!
}
