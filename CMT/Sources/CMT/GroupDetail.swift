//
//  Itemswift
//  MngApp
//
//  Created by Neeraj Bajaj
//  Copyright (c) 2015 WirelesslyLinked, LLC. All rights reserved.
//

import Foundation

class GroupDetail : Hashable, CustomStringConvertible {

    var objectId: String!

    var parentGroup: GroupDetail?
    
    var locId: LocationDetail!
    
    var parentGroupId:String!

    var groupType:String!

    var childGroups = [GroupDetail]()

    var childGroupIds = [String]()

    var groupName: String!
    
    var groupDesc: String!

    var itemIds = [String]()

    var items = [ItemDetail]()
    
    var openHours:[String:String]?
    
    var isProcessed = false


    convenience init(json: [String: Any], parentGroup: GroupDetail){
        
        self.init(json: json)
        self.parentGroup = parentGroup
        
        print( getSpaces() + "group: [" + self.parentGroup!.groupName! + "] -> " + self.groupName! )
        
        //print(" \(getSpaces()) group: [\(self.groupName!)] parentGroup: [\(self.parentGroup!.groupName!)] parentGroupId: \(self.parentGroupId)")
    }
    
    init(json: [String:Any]){

        self.groupName = json["groupName"] as? String
        
        self.objectId = json["objectId"] as? String
        self.groupType = json["groupType"] as? String
        
        if self.groupType != nil && (self.groupType == "nil" || self.groupType == ""){
            self.groupType = nil
        }
        
        self.groupDesc = json["groupDesc"] as? String
        self.openHours = json["openHours"] as? [String:String]
        
        
        if let parentGrpPtr = json["parentGroup"] as? [String: Any]{
            self.parentGroupId = parentGrpPtr["objectId"] as? String
            
            //NOTE: somehow there are empty parentGroup in test env.
            if (self.parentGroupId != nil && self.parentGroupId!.isEmpty){
                self.parentGroupId = nil
            }
        }
        
        //set and verify loc id
        
//        self.locationDetailPointer = getPointerFor(Pointer.LocationDetail, objectId:BusinessUser.locationId )


        /*
        self.locationDetailPointer = json["locationDetailPointer"] as? [String:Any]
        let locId = locationDetailPointer["objectId"] as! String
        assert(locId == BusinessUser.locationId, "\(locId) / \(BusinessUser.locationId) json: \(json)")
        */

        self.itemIds.removeAll()
        
        if let ids = json["itemIds"] as? [String] {
            
            itemIds.append(contentsOf: ids)
        }
        
        self.childGroupIds.removeAll()
        
        if let ids = json["childGroupIds"] as? [String] {
            
            childGroupIds.append(contentsOf: ids)
        }


        //NOTE: "groups" and "items" keys are defined in JSON file that is read by CreateMenu tool
        
        if let childGroups = json["groups"] as? [[String:Any]] {
            
            spaces = spaces + 4
            
            for childGroupJson in childGroups {
                
                createChildGroup(json: childGroupJson)
            }
            
            spaces = spaces - 4
        }
        
        if let itemsJson = json["items"] as? [[String:Any]] {
            
            spaces = spaces + 4
            
            for itemJson in itemsJson {
                
                createItem(json: itemJson)
            }
            
            spaces = spaces - 4
        }
        
        if config.actionType == .updateMenu {
            validateNonPrintableCharacters()
        }
    }
    
    
    // This method checks if  group name contains non printable characters
    /* List of non printable characters
     U+200B    Zero-Width Space
     U+200C    Zero Width Non-Joiner
     U+200D    Zero Width Joiner
     U+200E    Left-To-Right Mark
     U+200F    Right-To-Left Mar
     */
    
    func validateNonPrintableCharacters() {
    
        // validate group name for non printable chaaracters
        if groupName.containsNonPrintableCharacters(){
            debugPrint("Non printable characters are not allowed with group name: " + groupName)
            exit(1)
        }
    }
 
    
    // return the json that will be written to file
    func getJson() -> [String:Any] {
        
        var json = [String:Any]()
        
        if config.saveObjectId {
            json["objectId"] = objectId
        }
        
        json["groupName"] = groupName
        
        if let groupDesc = groupDesc{
            json["groupDesc"] = groupDesc
        }
        
        if let groupType = groupType{
            json["groupType"] = groupType
        }
        
        if let openHours = openHours {
            json["openHours"] = openHours
        }
        
        if config.saveObjectId && !childGroupIds.isEmpty{
            json["childGroupIds"] = childGroupIds
        }
        
        if config.saveObjectId && !itemIds.isEmpty{
            json["itemIds"] = itemIds
        }
        
        if !items.isEmpty {
            json["items"] = getItemsJson()
        }
        
        /* Note: no need to write "parentGroupId" it is determined
         from the parent in the JSON tree
         
         if let parentGroupId = parentGroupId {
         json["parentGroupId"] = parentGroupId
         }
         */
        
        if !childGroups.isEmpty{
            
            var childGroupsJson = [[String:Any]]()
            
            for childGroup in childGroups{
                
                childGroupsJson.append(childGroup.getJson())
            }
            
            json["groups"] = childGroupsJson
        }
        
        return json
    }
    
    //get items as json array
    func getItemsJson() -> [[String:Any]]{
        
        var json = [[String:Any]]()
        
        for item in items{
                        
            json.append(item.getJson())
            
        }
        
        return json
    }

    

        
    func populateItemsIds(){
        
        //we don't want to change the ordering if it has been set

        if !itemIds.isEmpty{
            
            return
        }
        
        itemIds.removeAll()
        
        for item in items {
            
            itemIds.append(item.itemId!)
        }
    }
    
    func populateChildGroupIds(){
        
        //we don't want to change the ordering if it has been set
        if !childGroupIds.isEmpty {
            return
        }
        
        childGroupIds.removeAll()
        
        for childGroup in childGroups {
            
            childGroupIds.append(childGroup.objectId)
        }
    }
    
    
    func createChildGroup(json:[String: Any]){
        
        let gd = GroupDetail(json: json, parentGroup: self)
        
        childGroups.append(gd)
    }
    
    func createItem(json: [String: Any]){
        
        let id = ItemDetail()
        id.set(json: json, groupDetail: self)
        
        self.items.append(id)
    }
    
        
    var description: String {
        
        return "groupId: \(objectId) groupName: \(groupName) childGroupIds: \(childGroupIds)"
    }

    var count: Int {
        return items.count
    }

    subscript (idx: Int) -> ItemDetail {
        return items[ idx ]
    }

    var hashValue: Int {
        get{
            return groupName!.hashValue
        }
    }

    func appendItemDetail( _ itemDetail: ItemDetail )
    {
        items.append( itemDetail )
    }

    func appendChildGroup( _ groupDetail: GroupDetail ){

        childGroups.append( groupDetail )
    }
    
}


func == (lhs: GroupDetail, rhs: GroupDetail) -> Bool {
    return lhs.groupName! == rhs.groupName!
}



