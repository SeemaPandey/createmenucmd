//
//  ItemDetail.swift
//  MngApp
//
//  Created by Neeraj Bajaj
//  Copyright (c) 2015 WirelesslyLinked, LLC. All rights reserved.
//

import Foundation

class ItemDetail : Hashable
{
    var itemId: String?
    var groupDetailPointer:[String:Any]!
    var groupId:String!
    
    var groupDetail:GroupDetail?
    
    var itemName: String!
    var itemDescription: String!
    var itemPrice: Double!
    var live: Bool!
    var option:[[String:Any]]?
    var mediaArray:[[String:Any]]?
    
    var isUpdated = false
    var delete = false
    
    init(){
        
        itemName = ""
        itemDescription = ""
        itemPrice = 0.0
        live = true
        option = [[String:Any]]()
    }
    
    
    func set(json: [String:Any], groupDetail: GroupDetail){
        
        self.groupDetail = groupDetail
        set(json: json)
        
        //print(" \(getSpaces()) item: [\(self.itemName!)] itemPrice: \(self.itemPrice) groupName: [\(self.groupDetail!.groupName!)]")
    }
    
    func set (json: [String:Any]){
        
    
        self.itemName = json["itemName"] as? String
        
        self.itemPrice = json["itemPrice"] as? Double
        
        //print("set() itemName: \(self.itemName!) itemPrice: \(self.itemPrice!)")
        
        self.itemId  = json["objectId"] as? String
        
        // NOTE: temporary workaround - for a new item objectId
        // should NOT be set
        if self.itemId != nil && self.itemId == ""{
            self.itemId = nil
        }
        
        self.live  = json["live"] as? Int == 0 ? false: true
        self.itemDescription  = json["itemDescription"] as? String
        
        self.option = json["optionCategories"] as? [[String:Any]]
        
        
        if (self.option != nil){
            
            // check the item and option category is duplicate or not
            // return all the list of duplicate item with option category name
            checkDuplicateOptionCategoryIsExist()///*optionCategories: self.option!, itemName: self.itemName*/
            
            validateItemOptionName()
            //assert ( validateItemOptionName(optionCategories: self.option!), " invalid parse key in \(self.option!)" )
        }
        
        self.mediaArray = json["mediaArray"] as? [[String:Any]]
        
        //set and verify loc id
        
//        self.locationDetailPointer = getPointerFor(Pointer.LocationDetail, objectId: BusinessUser.locationId)
        
        /*
         self.locationDetailPointer = json["locationDetailPointer"] as? [String:Any]
         let locId = locationDetailPointer["objectId"] as! String
         assert(locId == BusinessUser.locationId, "\(locId) / \(BusinessUser.locationId) json: \(json)")
         
         */
        
        self.groupDetailPointer = json["groupDetailPointer"] as? [String:Any]
        
        // if JSON file has "delete" : true - then delete this item
        if let del = json["delete"] as? Bool {
            
            self.delete = del
        }
        else{
            self.delete = false
        }
        
        if config.actionType == .updateMenu {
            validateNonPrintableCharacters()
        }
        
        
    }
    
    // This method checks if item, group, category and option name contains non printable characters
    /* List of non printable characters
        U+200B    Zero-Width Space
        U+200C    Zero Width Non-Joiner
        U+200D    Zero Width Joiner
        U+200E    Left-To-Right Mark
        U+200F    Right-To-Left Mar
    */
    func validateNonPrintableCharacters() {
        
        // validate hidden character for item name
        if itemName.containsNonPrintableCharacters(){
            debugPrint("Non printable characters are not allowed with item name: " + itemName)
            exit(1)
        }
        
        // validate group name for non printable chaaracters
        if groupDetail != nil && groupDetail!.groupName.containsNonPrintableCharacters(){
            debugPrint("Non printable characters are not allowed with group name: " + groupDetail!.groupName)
            exit(1)
        }
        
        // validate category and option name for non printable chaaracters
        if let option = option{
        for categoryJson in option {
            
            //validate category name for non printable chaaracters
            if let category = categoryJson["category"] as? String{
                if (category.containsNonPrintableCharacters()){
                    debugPrint("Non printable characters are not allowed with category name: " + category)
                    exit(1)
                }
            }
            
            // Validate Non printable character for item options
            if let options = categoryJson["options"] as? [Any] {
                
                for option in options {
                    
                    let itemOption = option as! [String: Any]
                    
                    let optionName = itemOption.first!.key
                    
                    if  optionName.containsNonPrintableCharacters() {
                        debugPrint("Non printable characters are not allowed with option name: " + optionName )
                        exit(1)
                    }
                }
            }
        }
        }
    
    }
    

    // itemOptionName key can't have . in it for ex. Dr. Pepper
    func validateItemOptionName() {
        
        guard let option = self.option else {
            return
        }
        
        for categoryJson in option {
            
            if let options = categoryJson["options"] as? [Any] {
                
                for option in options {
                    
                    let itemOption = option as! [String: Any]
                    
                    let optionName = itemOption.first!.key
                    
                    // check in the optionName if following char is exist then stop uploading menu process (, & .)
            
                    if optionName.containsNotAllowedCharacters() {
                        
                        debugPrint("Hyphen '-', dot ('.') characters are not allowed in option name " + optionName)
                        exit(1)
                    }
                }
            }
        }
    }
    
    
    // check the optionCategory if it found the duplicate option category
    //then it return true with item and options category
    func checkDuplicateOptionCategoryIsExist() {
        
        guard let optionCategories = self.option else {
            return
        }
        
        // here we can store dulicate elements in array
        var storeDuplicateValue =  [String:Int]()
        
        var count = 0
        // here we loop through all elements
        for categoryJson in optionCategories {
            let category = (categoryJson["category"] as? String)?.trimmingCharacters(in: .whitespaces)
            
            count = 0
            for newCategoryJson in optionCategories {
                let newCategory = (newCategoryJson["category"] as? String)?.trimmingCharacters(in: .whitespaces)
                if(category == newCategory){
                    count += 1
                }
            }
            
            // if count is more than 1 we have duplicate elements
            if(count > 1){
                // so we store duplicate elements in dictionary and its count to know how many times it has been repeated
                storeDuplicateValue.updateValue(count, forKey: category!)
            }
        }
        
        if storeDuplicateValue.keys.count > 0{
            // here we get count of keys in dictionary to know number of duplicate strings
            debugPrint("** Error: Duplicate Items and Option Category Names with count **")
            debugPrint("Item Name : ", self.itemName)
            debugPrint("Option Category : ", storeDuplicateValue)
            exit(1)
        }
        
      
    }
    
    func getJson() -> [String:Any] {
        
        var itemJson = [String:Any]()
        
        if config.saveObjectId && itemId != nil{
            
            itemJson["objectId"] = itemId!
        }
        
        itemJson["itemName"] = itemName
        
        let twoDecimalPrice = priceRoundedToTwoDecimal(itemPrice)
        itemJson["itemPrice"] = twoDecimalPrice
        
        //print("getItemsJson() itemName: \(itemName!) itemPrice: \(itemPrice!) twoDecimalPrice: \(twoDecimalPrice)")
        
        if itemDescription != nil {
            itemJson["itemDescription"] = itemDescription!
        }
        
        itemJson["live"] = live
        
        if mediaArray != nil{
            itemJson["mediaArray"] = mediaArray!
        }
        
        if option != nil{
            itemJson["optionCategories"] = option!
        }
        
        return itemJson
    }
    
    var hashValue: Int {
        get{
            return itemName!.hashValue
        }
    }
    
}

func == (lhs: ItemDetail, rhs: ItemDetail) -> Bool {
    return lhs.itemName! == rhs.itemName!
}



