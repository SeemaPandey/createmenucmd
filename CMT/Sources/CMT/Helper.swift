//
//  Helper.swift
//  CreateMenu
//
//  Created by Seema Pandey on 10/12/16.
//  Copyright © 2016 Wirelesslylinked. All rights reserved.
//

import Foundation

import Alamofire


//TODO: there is pending issue where double values are NOT written to 2 decimal places
// for ex. "itemPrice" : 9.949999999999999 rather than "itemPrice" : 9.95
/*
 var businessId:String!
 var locId: String!
 */

var spaces = 0


let fileExtension = ".json"

func getSpaces() -> String {
    
    return String(repeating: " ", count: spaces)
}

func getParseAppId() -> String {
    
    switch config.env {
        
    case "prod2":
        return "ONChr2vcewJxAhcy2Lr9mBfxLuaupfApRj9UUDdg"
        
    case "dev2":
        return "wlparse-test-app-id"
        
    default:
        assert(true, "unsupported env: \(config.env)")
        return ""
        
    }
}

func getParseServerURL() -> String {
    
    switch config.env {
        
    case "prod2":
        return "https://wlparse-server.herokuapp.com/parse"
        
    case "dev2":
        return "https://wlparse-test.herokuapp.com/parse"
        
    default:
        assert(true, "unsupported env: \(config.env)")
        return ""
    }
}

func getParseHost() -> String {
    
    switch config.env {
        
    case "prod2":
        return "wlparse-server.herokuapp.com"
        
    case "dev2":
        return "wlparse-test.herokuapp.com"
        
    default:
        assert(true, "unsupported env: \(config.env)")
        return ""
    }
}

func getWLCloudURL() -> String {
    
    switch config.env {
        
    case "prod2":
        return "https://wlcloud.herokuapp.com"
        
    case "dev2":
        return "https://wlcloud-dev2.herokuapp.com"
        
    default:
        assert(true, "unsupported env: \(config.env)")
        return ""
    }
}

enum Pointer:String {
    case BusinessDetail = "BusinessDetail"
    case LocationDetail = "LocationDetail"
    case ItemDetail = "ItemDetail"
    case GroupDetail = "GroupDetail"
}

//
enum FileType:String{
    case original
    case update
}


func getPointerFor(_ pointerName: Pointer, objectId: String) -> [String: Any] {
    
    var pointer = [String: Any]()
    pointer ["__type"] = "Pointer"
    pointer ["objectId"] = objectId
    pointer ["className"] = pointerName.rawValue
    return pointer
}

/*
 
 "ACL":{
 "*":{
 "read":true
 },
 "role:fouB2BQ91e_ADMIN":{
 "read":true,
 "write":true
 }
 },
 
 
 func getACL() -> [String : Any] {
 
 let role = "role:" + BusinessUser.locationId + "_ADMIN"
 
 var acl = [String: Any]()
 
 acl["*"] =  ["read" : true]
 acl[role] = ["read" : true, "write" : true]
 
 return acl
 }
 */



var timestamp: String {
    return "\(NSDate().timeIntervalSince1970)"
}


func createItemOption(_ categoryName:String,options:[String:Double],required:Bool,minChoose:Int,maxChoose:Int) -> [String:Any]{
    
    var opt = [String:Any]()
    opt["category"] = categoryName
    opt["maxChoose"] = maxChoose
    opt["minChoose"] = minChoose
    opt["required"] = required
    for itemOp in options{
        opt[itemOp.0] = itemOp.1
    }
    return opt
}

func stringWithoutSpace(string:String) -> String{
    return string.replacingOccurrences(of: " ", with: "")
}



func readJSONFile(filePath: String) -> [String: Any]?{
    
    do {
        
        let data = try Data(contentsOf: URL(fileURLWithPath: filePath))
        
        let responseJSON = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
        
        return responseJSON
        
        
    } catch {
        
        print("ERROR: catch \(error.localizedDescription)")
        
    }
    
    return nil
}

/*
 func getFilePath(fileName:String,type:FileType) -> String{
 
 //var filePath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
 
 let fileName = stringWithoutSpace(string: fileName)
 
 //var path = filePath[0].appending("/" + fileName)
 var path = fileName
 
 switch type {
 
 case .original:
 path = path + fileExtension
 if !(FileManager.default.fileExists(atPath: path)){
 FileManager.default.createFile(atPath: path, contents: nil, attributes: nil)
 }
 
 case .update:
 path = path + "update" + fileExtension
 if !(FileManager.default.fileExists(atPath: path)){
 // File to update items
 FileManager.default.createFile(atPath: path , contents: nil, attributes: nil)
 }
 }
 
 // location of file
 print("JSON File Path = " + path)
 return path
 }
 */
// Decimal Handler
let decimalHandler = NSDecimalNumberHandler(roundingMode: NSDecimalNumber.RoundingMode.plain, scale: 2, raiseOnExactness: false, raiseOnOverflow: false, raiseOnUnderflow: false, raiseOnDivideByZero: false)


func priceRoundedToTwoDecimal(_ price:Double) -> Double {
    
    
    let scaledNumber = NSDecimalNumber(value: price).rounding(accordingToBehavior: decimalHandler)
    return scaledNumber.doubleValue
    
    /*
     let formatter = NumberFormatter()
     formatter.numberStyle = NumberFormatter.Style.currency
     formatter.locale = Locale(identifier: "en_US")
     
     // Rounding Double value to
     let formattedPriceStr = formatter.string(from: NSDecimalNumber(value: price as Double).rounding(accordingToBehavior: decimalHandler))!
     
     let currencySynmbol = formatter.currencySymbol
     let priceStrWithoutSymbol = formattedPriceStr.replacingOccurrences(of: currencySynmbol!, with: "")
     let priceDouble =  priceStrWithoutSymbol.toDouble()
     
     return (formattedPriceStr,priceDouble!,priceStrWithoutSymbol)
     */
}

func printGroup(group: GroupDetail){
    
    if group.groupType != nil && group.groupType == "MENU" {
        
        print("MENU: \(group.groupName!) [\(group.childGroups.count)]")
        printChildGroups(group: group, spacesCount: 4)
    }
    else{
        
        print("\(group.groupName!) [\(group.items.count)]")
        printSpaces(count: 4)
        printGroupItems(group: group)
    }
}

func printChildGroups(group: GroupDetail, spacesCount: Int){
    
    for group in group.childGroups {
        
        printSpaces(count: spacesCount)
        
        //ex. Seafood [4]
        print("\(group.groupName!) [\(group.items.count)]")
        printSpaces(count: spacesCount + 4)
        printGroupItems(group: group)
    }
    
}


func printGroupItems(group: GroupDetail){
    
    var itemStrings = [String] ()
    
    for item in group.items {
        
        itemStrings.append(item.itemName)
    }
    
    print(itemStrings)
}

func printSpaces(count: Int){
    
    for _ in 0..<count{
        print(" ", separator: "", terminator: "")
    }
}



// replace the space in file name with underscore '_' and adds .json extension
func getFileName(name: String) -> String {
    
    var strippedName = name.replacingOccurrences(of: " ", with: "_")
    strippedName.append(".json")
    return strippedName
}



//
// MARK: read/write menu from/to file.
//

//write JSON format to given file
func writeJsonToFile(json: [String: Any], filePath: String) throws {
    
    let jsonData = try JSONSerialization.data(withJSONObject: json, options: JSONSerialization.WritingOptions.prettyPrinted)
    
    try jsonData.write(to: URL(fileURLWithPath: filePath), options: Data.WritingOptions.atomic)
}

//read JSON data from given file
func readJsonFromFile(filePath: String) throws -> [String: Any]? {
    
    let data = try Data(contentsOf: URL(fileURLWithPath: filePath))
    
    return try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.allowFragments) as? [String:Any]
}


func getFilePath(fileName: String) -> String {
    
    //absolute file path
    let filePath = config.menuDir! + "/" + fileName
    
    return filePath
}

/*
// check the optionCategory if it found the duplicate option category
//then it return true with item and options category
func checkDuplicateOptionCategoryIsExist(optionCategories:[[String:Any]],itemName:String) -> Bool {
    
    // here we can store dulicate elements in array
    var storeDuplicateValue =  [String:Int]()
    
    var count = 0
    // here we loop through all elements
    for categoryJson in optionCategories {
        let category = (categoryJson["category"] as? String)?.trimmingCharacters(in: .whitespaces)
        
        count = 0
        for newCategoryJson in optionCategories {
            let newCategory = (newCategoryJson["category"] as? String)?.trimmingCharacters(in: .whitespaces)
            if(category == newCategory){
                count += 1
            }
        }
        
        // if count is more than 1 we have duplicate elements
        if(count > 1){
            // so we store duplicate elements in dictionary and its count to know how many times it has been repeated
            storeDuplicateValue.updateValue(count, forKey: category!)
        }
    }
    
    if storeDuplicateValue.keys.count > 0{
        // here we get count of keys in dictionary to know number of duplicate strings
        print("** Error: Duplicate Items and Option Category Names with count **")
        print("Item Name : ", itemName)
        print("Option Category : ", storeDuplicateValue)
        // print(storeDuplicateValue.keys.count)
        
        return true
    }
    
    //print("** No duplicate Items and Options Category found **")
    return false
}
*/

extension Character{
    var isNonPrintable: Bool{
        switch unicodeScalars.first {
        case "\u{200B}","\u{200C}","\u{200D}","\u{200E}","\u{200F}":
            return true
        default:
            return false
        }
    }
}

extension String {
    func containsNonPrintableCharacters() -> Bool {
        return self.filter{ $0.isNonPrintable }.count > 0
    }
    
    func containsNotAllowedCharacters() -> Bool {
        return (self.contains(".") || self.contains("_"))
    }
    
}


