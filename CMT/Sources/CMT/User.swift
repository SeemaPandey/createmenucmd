   
   struct BusinessDetailPointer: Codable{
    var objectId: String?
   }
   
   struct LocationDetailPointer: Codable{
    var objectId: String?
   }
   
   
   struct User: Codable {
    
    var business: BusinessDetailPointer?
    
    var location: LocationDetailPointer?
    
    var userId: String?
    
    var session: String?
    
    enum CodingKeys: String, CodingKey {
        case userId = "objectId"
        case session = "sessionToken"
        case location = "locationDetailPointer"
        case business = "bizId"
    }
    
    static func login(){
        
    }
    
    static func logoutAll(){
        
        //PFUser.logOut()
    }
    
   }
   
