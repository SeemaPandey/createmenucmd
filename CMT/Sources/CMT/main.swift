import Foundation
//import Parse

//let email = CommandLine.arguments[1]
//let pass = CommandLine.arguments[2]
//login(email:email,password:pass)



// Login User in main thread
// Get locId configured on the user
// Write all group menus
// update single menu



struct Config {
    
    var env = "dev2"
    var userName = ""
    var password = ""
    var bizId: String?
    var locId: String?
    var menuDir: String?
    var actionType = ActionType.downloadMenu
    
    // default saveObjectId is true
    // this is useful if we want to save menu of given location as a new menu which can be updateed for a different location
    var saveObjectId = true
    
    var isMenuSameForAllLocation = true
}

enum ActionType: String {
    
    case updateBiz
    case downloadBiz
    case createLoc
    case downloadLoc
    case updateLoc
    case downloadMenu
    case updateMenu
    case deleteMenu
}

var location: LocationDetail?
var business: BusinessDetail?

var user = User()
var config = Config()


// get Config object from user input
func getConfig() -> Config {

    var cfg = Config()
    
    debugPrint("Enter env (dev2|prod2): ", separator: "", terminator: "")
    
    cfg.env = readLine()!
    
    if cfg.env != "dev2" && cfg.env != "prod2" {
        
        debugPrint("env must be either dev or dev2 or prod2")
        exit(1)
    }

    
    let menuDirEnvVar = getenv("MENU_DIR")
    
    if menuDirEnvVar == nil {
        
        debugPrint("Set the MENU_DIR environment variable")
        //exit(1)
    }
    
    cfg.menuDir = String(utf8String: menuDirEnvVar!)
    
    
    debugPrint("Menu directory: \(cfg.menuDir!)")
    
    debugPrint("Is this correct?: (y|n) ", separator: "", terminator: "")
    var input = readLine()!
    
    if input != "y" {
        
        debugPrint("Set the MENU_DIR environment variable")
        exit(1)
    }
    
    debugPrint("Enter actionType: (\(ActionType.downloadBiz) | \(ActionType.updateBiz) | \(ActionType.createLoc) | \(ActionType.downloadLoc) | \(ActionType.updateLoc) | \(ActionType.downloadMenu) | \(ActionType.updateMenu) | \(ActionType.deleteMenu) ): ", separator: "", terminator: "")
    
    cfg.actionType = ActionType(rawValue: readLine()!)!

    // save object id?
    
    if cfg.actionType == .downloadLoc || cfg.actionType == .downloadMenu {
        
        if cfg.actionType == .downloadMenu {
            
            readBizId()
            
            readLocIdIfMenuIsSameForAllLocation()
            
        }
        
        debugPrint("save object id?: (y|n) ", separator: "", terminator: "")
        
        input = readLine()!
        
        if input == "y" {
            
            cfg.saveObjectId = true
        }
        else{
            
            cfg.saveObjectId = false
            
            debugPrint("This would write data without any object id. Are you sure this is what you want to do? (y|n)", separator: "", terminator: "")
            
            let input2 = readLine()!
            
            if input2 != "y"{
                exit(1)
            }
        }
    }
    
    if cfg.actionType == .downloadBiz {
        
        readBizId()
    }
    else if cfg.actionType == .downloadLoc {
        
        readLocId()
    }
    
    return cfg
}

func readLocIdIfMenuIsSameForAllLocation(){
    
    debugPrint("Is menu same for all location? (y|n)", separator: "", terminator: "")
    
    let input = readLine()!
    
    if input == "n"{
        config.isMenuSameForAllLocation = false
        readLocId()
    }
}

func readBizId(){
    
    debugPrint("Enter bizId: ", separator: "", terminator: "")
    if user.business == nil{
        user.business = BusinessDetailPointer()
    }
    user.business!.objectId = readLine()!
}

func readLocId(){
    
    debugPrint("Enter locId: ", separator: "", terminator: "")
    if user.location == nil{
        user.location = LocationDetailPointer()
    }
    user.location!.objectId = readLine()!
}


//MARK: login user

// logs in the user and sets BusinessUser.businessId and BusinessUser.locationId

func loginUser(completion:@escaping ( _ error:String?) -> Void){
    
    debugPrint("Enter user name: ", separator: "", terminator: "")
    config.userName = readLine()!
    
    debugPrint("Enter password: ", separator: "", terminator: "")
    config.password = readLine()!

    CloudRequests.loginUser(completionHandler: { (response:[String : AnyObject]?, error:String?) in
        
        if error != nil {
            config.userName = ""
            debugPrint("ERROR: logInWithUsername() error: \(error)")
            return
        }
        debugPrint("bizid: \(user.business?.objectId!)")
        
        debugPrint("locId: \(user.location?.objectId!)")
        
        completion(nil)
        
    }
    )
    
}



//MARK: Location (update, download)

func updateLocation(isNewLocation: Bool){
    
    debugPrint("\nEnter LocationDetail file: ", separator: "", terminator: "")
    
    let fileName = readLine()!
    
    if !isNewLocation && !fileName.hasSuffix("copy.json"){
        
        debugPrint("ERROR: filename must end with copy.json")
        updateLocation(isNewLocation: false)
    }
    
    let filePath = getFilePath(fileName: fileName)
    
    do{
        
        let jsonData = try readJsonFromFile(filePath: filePath)
        
        let loc = LocationDetail()
        loc.set(json: jsonData!)
        
        if isNewLocation{
            
            CreateMenus.createLocation(location: loc, createLocCompleted: { (createLocError: String?) in
                
                if createLocError != nil{
                    
                    debugPrint("ERROR: creating location. file: " + filePath + " error: " + createLocError!)
                    return
                }
                
                debugPrint("\nSUCCESS: created location. file: " + filePath)
            })
            
        }
        else{
            
            CreateMenus.updateLocation(location: loc, updateLocCompleted: { (updateLocError: String?) in
                
                if updateLocError != nil{
                    
                    debugPrint("ERROR: updating location. file: " + filePath + " error: " + updateLocError!)
                    return
                }
                
                debugPrint("\nSUCCESS: updated location. file: " + filePath)
            })
        }
        
        
    }
    catch{
        
        debugPrint("ERROR: readMenuFromFile() " + error.localizedDescription)
    }
}

func downloadLocation(block: @escaping (String?) -> Void){
    
//    assert(BusinessUser.locationId != nil)
    
//    location = LocationDetail(id: BusinessUser.locationId)
//    assert(user.location.objectId != nil)
    guard let locationDetailPointer = user.location else {
        debugPrint("Location Id is not present")
        exit(1)
        return
    }
    
    debugPrint("user.location.objectId = ", locationDetailPointer.objectId!)
    location = LocationDetail(id: locationDetailPointer.objectId!)
    
    location!.downloadLocation { (downloadLocError: String?) in
        
        if downloadLocError != nil{
            let errMsg = "download location error: " + downloadLocError!
            debugPrint(errMsg)
            block(errMsg)
            return
        }
        
        debugPrint("Location downloaded.")
        
        do{
            
            try location!.writeLocationToFile()
            block(nil)
            
        }
        catch{
            
            let errMsg = "ERROR: writeLocationToFile() " + error.localizedDescription
            debugPrint(errMsg)
            block(errMsg)
        }
        
    }
}

//MARK: Menu (download, update, delete)

func downloadMenu(block: @escaping (String?) -> Void){
    
    guard let userBusinesPointer = user.business else {
        
        debugPrint("Business Id is not present")
        exit(1)
    
    }
    
//    assert(user.business.objectId != nil)
    
    //location = LocationDetail(id: BusinessUser.locationId)
    business = BusinessDetail(id: userBusinesPointer.objectId!)
    
    business!.downloadMenu { (error:String?) in
        
        if error != nil {
            
            debugPrint("Error: \(error!)")
            block(error!)
            return
        }
        
        debugPrint("Menu downloaded")
        
        //location!.printMenu()
        business!.printMenu()
        
        do{
            
            //try location!.writeAllMenusToFile()
            try business!.writeAllMenusToFile()
            block(nil)
        }
        catch{
            
            let errMsg = "ERROR: writeAllMenusToFile() " + error.localizedDescription
            debugPrint(errMsg)
            block(errMsg)
        }
        
    }
    
}


func updateMenu(){
    
    debugPrint("\nEnter MENU file to update: ", separator: "", terminator: "")
    
    let fileName = readLine()!
    
    if !fileName.hasSuffix("copy.json"){
        
        debugPrint("ERROR: filename must end with copy.json")
        updateMenu()
    }
    
    let filePath = getFilePath(fileName: fileName)
    
    do{
        
        let menuAsJson = try readJsonFromFile(filePath: filePath)
        
        let group = GroupDetail(json: menuAsJson!)
        
        //print group
        printGroup(group: group)
        
        CreateMenus.processGroup(group: group, operationCompleted: { (error: String?) in
            
            if error != nil {
                
                debugPrint("ERROR: updating file: " + fileName + " error: " + error!)
                return
            }
         
            debugPrint("\nSUCCESS: updated file: " + fileName)
            
            updateMenu()
        })
        
    }
    catch{
        
        debugPrint("ERROR: readMenuFromFile() " + error.localizedDescription)
    }
    
}

func deleteMenu(){
    
    inputLocId()
    
    var i = 0
    
    for group in business!.groupDetailArray {
        
        debugPrint("\(i). " + group.groupName)
        
        i = i + 1
    }
    
    let exit = "exit"
    
    debugPrint("\nType \(exit) to quit. Enter group name you want to delete. : ", separator: "", terminator: "")
    
    let groupName = readLine()!
    
    if groupName == exit {
        
        return
    }
    
    for (idx, group) in business!.groupDetailArray.enumerated() {
        
        if group.groupName == groupName {
            
            CreateMenus.deleteGroup(group: group, operationCompleted: { (error: String?) in
                
                if error != nil {
                    
                    debugPrint("ERROR: deleteGroup \(group.groupName!) error: \(error!)")
                    return
                }
                
                debugPrint("SUCCESS: \(group.groupName!) deleted.")
                
                business!.groupDetailArray.remove(at: idx)
                
                deleteMenu()
                
            })
            
            return
        }
    }
    
    debugPrint("You entered invalid group: \(groupName). Try again!\n")
    
    deleteMenu()
}


//MARK: Business (update, download)

func updateBusiness(){
    
    debugPrint("\nEnter BusinessDetail file to update: ", separator: "", terminator: "")
    
    let fileName = readLine()!
    
    if !fileName.hasSuffix("copy.json"){
        
        debugPrint("ERROR: filename must end with copy.json")
        updateBusiness()
    }
    
    let filePath = getFilePath(fileName: fileName)
    
    do{
        
        let jsonData = try readJsonFromFile(filePath: filePath)
        
        let biz = BusinessDetail()
        biz.set( jsonData! )
        
        CreateMenus.updateBusiness(biz: biz, updateBizCompleted: { (updateBizError: String?) in
            
            if updateBizError != nil {
                
                debugPrint("ERROR: updated business. file: " + filePath + " error: " + updateBizError!)
                return
            }
            
            debugPrint("\nSUCCESS: updated business. file: " + filePath)
        })
        
    }
    catch{
        
        debugPrint("ERROR: readMenuFromFile() " + error.localizedDescription)
    }
}


func downloadBusiness(block: @escaping (String?) -> Void){
    
    
    guard let userBusinesPointer = user.business else {
        
        debugPrint("Business Id is not present")
        exit(1)
        
    }
//    assert(user.business.objectId != nil)
    
    let biz = BusinessDetail(id: userBusinesPointer.objectId!)
    biz.downloadBusiness { (downloadBizError: String?) in
        
        if downloadBizError != nil {
            
            let errMsg = "download business error: " + downloadBizError!
            debugPrint(errMsg)
            block(errMsg)
            return
        }
        
        debugPrint(biz.bizId! + " BusinessDetail downloaded.")
        
        do {
            
            try biz.writeBusinessDetailToFile()
            block(nil)
            
        }
        catch {
            
            let errMsg = "ERROR: writeBusinessDetailToFile() " + error.localizedDescription
            debugPrint(errMsg)
            block(errMsg)
        }
    }
}


func inputLocId(){
    
    
    debugPrint("Enter locId: ", separator: "", terminator: "")
    
    let locId = readLine()!
    
    debugPrint("Enter locId again: ", separator: "", terminator: "")
    
    let locId2 = readLine()!
    
    if locId != locId2 {
        
        debugPrint("locId mismatch!")
        exit(1)
    }
    
    if user.location == nil{
        user.location = LocationDetailPointer()
    }
    user.location!.objectId = locId
    
    debugPrint("locId updated to: " + locId)
    
}


config = getConfig()

func performActionAfterUserLogin()  {
    
    switch config.actionType {
        
   
    case .updateMenu:
        
        
            readLocIdIfMenuIsSameForAllLocation()
            
            // Download menu
            downloadMenu(block: { (error: String?) in
                
                if error != nil{
                    return
                }
                
                updateMenu()
                
            })
        
        
    case .deleteMenu:
        
        readLocIdIfMenuIsSameForAllLocation()
        
        downloadMenu(block: { (error: String?) in
            
            if error != nil{
                return
            }
            
            deleteMenu()
            
        })
        
    case .createLoc:
        
        updateLocation(isNewLocation: true)
        

    case .updateBiz:
        
        downloadBusiness(block: { (error: String?) in
            
            if error != nil{
                return
            }
            
            updateBusiness()
        })
        
    case .updateLoc:
        
        inputLocId()
        
        downloadLocation(block: { (error: String?) in
            
            if error != nil{
                return
            }
            
            updateLocation(isNewLocation: false)
            
        })
        
    default:
        print("Do nothing")
        
    }
    
}


switch config.actionType {
    
case .downloadMenu:
    
    downloadMenu(block: { (error: String?) in
        
        // no-op
    })
    
case .updateMenu,.deleteMenu,.createLoc, .updateBiz, .updateLoc:
    
    loginUser(completion:{(error: String?) in
      performActionAfterUserLogin()
    })


case .downloadLoc:
    
    downloadLocation(block: { (error: String?) in
        
        // no-op
    })
    
    
case .downloadBiz:
    
    downloadBusiness(block: { (error: String?) in
        
        // no-op
    })
    
}

//downloadMenu()

//Don't delete or change this line
RunLoop.main.run(until: Date(timeIntervalSinceNow: 500000000))

