//
//  BusinessDetail.swift
//  CreateMenu
//
//  Created by Seema Pandey on 11/7/16.
//  Copyright © 2016 Wirelesslylinked. All rights reserved.
//

import Foundation
import Alamofire

class BusinessDetail {
    
    var bizId: String?
    var isLive: Bool!
    var stopOO: Bool!
    var name: String?

    var logoURL: String?
    var media:Media?
    var usrAppConfig: Dictionary<String, AnyObject>?
    var mngAppConfig: Dictionary<String, AnyObject>?
    var webConfig: Dictionary<String, AnyObject>?
    var bizConfig: Dictionary<String, AnyObject>?
    var websiteConfig = [String:Any]()

    
    var groupIds = [String]()

    // Parent Groups
    var groupDetailArray = [GroupDetail]()

    //all groups (including sub-group). key is groupId.
    var groupDictionary = [String: GroupDetail] ()
    
    //all items that belong to a  key is itemId.
    var itemDetailDictionary = [ String: ItemDetail ] ()
    
    var locIds = [String]()

    var isMenuSameForAllLocation = true

    init(){
        
    }
    
    init(id:String){
        self.bizId = id
    }
    
    func set(_ json : [String: Any?]){
        
        self.bizId = (json["objectId"] as! String)
        self.name = json["name"] as? String
        self.isLive = (json["isLive"] as? Bool) ?? false
        self.stopOO = (json["stopOO"] as? Bool) ?? false
        
        self.usrAppConfig = json["usrAppConfig"] as? [String: AnyObject]
        self.mngAppConfig = json["mngAppConfig"] as? [String: AnyObject]
        
        self.webConfig = json["webConfig"] as? [String: AnyObject]
        
        if let sURL = json["logoURL"] as? String {
            self.logoURL = sURL
            
        }
        
        if let locationIds = json["locIds"] as? [String]{
            self.locIds = locationIds
        }
        
        if let grpIds = json["groupIds"] as? [String]{
            self.groupIds = grpIds
        }

        if let websiteConfig = json["websiteConfig"] as? [String : AnyObject]{
            self.websiteConfig = websiteConfig
        }
        
        if let bizConfig = json["bizConfig"] as? [String : AnyObject]{
           
            self.bizConfig = bizConfig
            
            config.isMenuSameForAllLocation = bizConfig["isMenuSameForAllLocation"] as? Bool ?? true
        }
            
    }
    
    func getJson() -> [String:Any]{
        
        var json = [String:Any]()
        
        json["objectId"] = bizId!
        json["isLive"] = isLive
        json["stopOO"] = stopOO
        json["logoURL"] = logoURL
        json["usrAppConfig"] = usrAppConfig
        json["mngAppConfig"] = mngAppConfig
        json["webConfig"] = webConfig
        json["websiteConfig"] = websiteConfig
        json["locIds"] = locIds
        json["bizConfig"] = bizConfig
        
        if config.saveObjectId && !groupIds.isEmpty {
            json["groupIds"] = groupIds
        }

        return json
    }      
    
    func downloadBusiness( complete: @escaping (String?) -> Void) {
        
        CloudRequests.getAllLocations(bizId!) { (response, error) in
            
            if error != nil {
                
                complete(error!)
                return
            }
            
            let bizData = response!.object(forKey: "data") as! [String: AnyObject]
            self.set( bizData["BusinessDetail"] as! [String: AnyObject] )
            complete(nil)
        }
    }
    
    func downloadGroupDetails(complete:@escaping (String?)->Void){
        
        CloudRequests.getGroupDetails { (response:AnyObject?, error:String?) in
            
            if error != nil {
                
                complete(error!)
                return
            }
            
            let groupDetailObjects = response!.object(forKey: "data") as! [ AnyObject]
            
            for groupDetailObject in groupDetailObjects{
                
                let group = GroupDetail(json: groupDetailObject as! [String:AnyObject])
                //group.location = self
                self.groupDictionary[group.objectId!] = group
            }
            self.setGroupHierarchy()
            complete(nil)
        }
    }
    
    func downloadItemDetails(itemDetailsDownload: @escaping ( _ error: String?) -> Void )
    {
        
        CloudRequests.getItemDetails { (response, error) in
            
            if error == nil {
                
                let objects = response!.object(forKey: "data") as! [AnyObject]
                
                for itemDetailJson in objects {
                    
                    let itemDetail = ItemDetail()
                    //itemDetail.location = self
                    itemDetail.set(json:itemDetailJson as! [String:AnyObject])
                    
                    let groupId = itemDetail.groupDetailPointer["objectId"] as! String
                    itemDetail.groupDetail = self.groupDictionary[ groupId ]
                    
                    if itemDetail.groupDetail != nil {
                        
                        itemDetail.groupDetail!.appendItemDetail( itemDetail)
                        self.itemDetailDictionary[ itemDetail.itemId!] = itemDetail
                    }
                    else{
                        print("ERROR: item without any group. item JSON: \(itemDetailJson)")
                    }
                    
                }
                
                itemDetailsDownload(nil )
            }
            else{
                itemDetailsDownload(error)
            }
        }
    }

    func populateGroupIds(){
        
        //we don't want to change the ordering if it has been set
        if !groupIds.isEmpty{
            return
        }
        
        groupIds.removeAll()
        
        for group in groupDetailArray {
            
            groupIds.append(group.objectId!)
        }
    }
    
    
    func downloadMenu(complete:@escaping (String?) -> Void) {
        
        self.downloadBusiness { (downloadBusinessError: String?) in
            
            if downloadBusinessError != nil {
                
                complete(downloadBusinessError!)
                return
            }
            
            self.downloadGroupDetails{ (downloadGroupError:String?) in
                
                if downloadGroupError != nil{
                    
                    complete(downloadGroupError!)
                    return
                }
                
                self.downloadItemDetails { (downloadItemError) in
                    
                    if downloadItemError != nil {
                        
                        complete(downloadItemError)
                        return
                    }
                    
                    complete(nil)
                }
            }
        }
        
    }
    

    func writeBusinessDetailToFile() throws {
        
        var fileName = self.name!
        fileName.append("-bizId-")
        fileName.append(self.bizId!)
        
        let filePath = getFilePath(fileName: getFileName(name: fileName))
        print("Writing BusinessDetail to file: " + filePath)
        try writeJsonToFile(json: getJson(), filePath: filePath)
    }
    
    //this function should be called, once all groups have been downloaded
    func setGroupHierarchy(){
        
        let groupDic = self.groupDictionary
        
        var groupIterator = groupDic.makeIterator()
        
        var entry = groupIterator.next()
        
        while entry != nil {
            
            let group = entry!.value
            
            if let parentGroupId = group.parentGroupId {
                
                if let parentGroup = groupDic[ parentGroupId ] {
                    
                    parentGroup.appendChildGroup(group)
                }
                else{
                    
                    print("ERROR: couldn't find parentGroupId: \(parentGroupId) groupName: \(group.groupName) groupId: \(group.objectId!)")
                }
                
            }
            else{
                // if no parent, this group is a top level group
                self.groupDetailArray.append(group)
            }
            
            entry = groupIterator.next()
        }
    }
    
    
    func printMenu() {
        
        for group in groupDetailArray {
            
            printGroup(group: group)
            print()
        }
    }
    
    // write all menus (Dinner, Lunch etc.) for this business
    func writeAllMenusToFile() throws {
        
        for group in groupDetailArray {
            
            var fileName = self.name!
            
            if config.saveObjectId {
                if !config.isMenuSameForAllLocation && user.location == nil && user.location!.objectId == nil{
                    debugPrint("Menu is not same for all location. Please try again")
                    exit(1)
                }
                fileName.append(config.isMenuSameForAllLocation ? "-bizId-" : "-locId-")
                fileName.append(config.isMenuSameForAllLocation ? user.business!.objectId! : user.location!.objectId!)
            }
            
            fileName.append("-")
            let groupName = group.groupName!
            let groupNameNew = groupName.replacingOccurrences(of: "/", with: "-")
            fileName.append(groupNameNew)
            
            let filePath = getFilePath(fileName: getFileName(name: fileName))
            
            print("Writing to file: " + filePath)
            
            try writeJsonToFile(json: group.getJson(), filePath: filePath)
        }
    }
    
    /* Number of ItemDetail currently in memory. If itemCount < count, not all items have been downloaded. */
    var count: Int {
        return groupDetailArray.count
    }
    
    subscript (idx: Int) -> GroupDetail {
        return groupDetailArray[ idx ]
    }



    
}
