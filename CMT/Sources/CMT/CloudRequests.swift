//
//  CloudRequests.swift
//  MngApp
//
//  Created by Neeraj Bajaj on 8/11/16.
//  Copyright © 2016 WirelesslyLinked, LLC. All rights reserved.
//

import Foundation
import Alamofire

let ERROR_MSG_INVALID_USER_SESSION = "Invalid User Session. Please logout and login again."

class CloudRequests {
    
    static let wlCloudURL: String = getWLCloudURL()
    
    static let webKey: String = "TEST-123"
    
    static var userId: String?{
        return user.userId
    }
    
    static var userSession:String?{
        return user.session
    }
    
    class func isUserSessionInvalid() -> Bool {
        return (userId == nil || userSession == nil)
    }
    
    class func loginUser(completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void)  {
        let headers = [
            "X-Parse-Application-Id": getParseAppId(),
            "X-Parse-Revocable-Session": "1",
            "Accept": "*/*",
            "Host": getParseHost(),
            ]
        
        let urlStr = getParseServerURL() + "/login?username=" + config.userName + "&password=" + config.password
        
        let url = NSURL(string: urlStr) as! URL
        
        let request = NSMutableURLRequest(url: url,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let dataTask = URLSession.shared.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                completionHandler(nil , error!.localizedDescription)
            } else {
                
                if let jsonData = data{
//                    print(jsonData)
                    let decoder = JSONDecoder()
                    user = try! decoder.decode(User.self, from: jsonData)
                    print("Codable protocol is working perfect = ", user.userId, user.session, user.business?.objectId, user.location?.objectId)
                    completionHandler(nil,nil)
                }else{
                    completionHandler(nil,"loginUser: No data found")
                }
            
            }
        })
        
        dataTask.resume()
    }
    
//    class func login() {
//
//        let urlString = getParseServerURL() + "/login"
//
//        var requestParams: Parameters  = ["username": config.userName, "password": config.password]
//
//        var requestHeaders: Parameters = ["X-Parse-Application-Id": getParseAppId(), "X-Parse-Revocable-Session" : "1",  "Accept": "*/*", "Host": "wlparse-server.herokuapp.com"]
//
//        Alamofire.request(urlString, parameters: requestParams , encoding: JSONEncoding.default, headers: requestHeaders).responseJSON { response in
//
//            if response.result.isSuccess {
//
//                let result = response.result.value! as! [String: AnyObject]
//
//                if let errorMsg = result["error"] as? [String: AnyObject]{
//
//                    print("Error in Loggin = ",errorMsg)
//                }
//                else{
//                    print("Loggin response = ",response.result.value)
//                }
//
//            }
//            else{
//                print("Error in Loggin = ",errorMsg)
//            }
//        }
//
//    }
    
    //MARK: /ItemDetail
    
    // Create and Update item for location
    
    class func updateItemDetail(_ isNew: Bool, params: [String: Any], completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        let urlString = wlCloudURL + "/ItemDetail"
        
        var requestParams = [String: AnyObject]()
        
        requestParams["functionName"] = (isNew ? "createItemDetail" : "updateItemDetail") as AnyObject
        requestParams["userId"] = userId! as AnyObject
        requestParams["userSession"] = userSession! as AnyObject
        requestParams["params"] = params as AnyObject
        
        print("updateItemDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams , encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as? [String:AnyObject] , nil)
                }
                
            }
            else{
                
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    // Returns Item Detail
    class func getItemDetail(_ itemId: String, completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void){
        
        let urlString = wlCloudURL + "/ItemDetail"
        let requestParams = ["functionName":"getItemDetail", "params" : ["objectId":itemId]] as Parameters
        
        print("getItemDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value!  as? [String:AnyObject], nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    //getItemDetail for business
    class func getItemDetails(completionHandler: @escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        let urlString = wlCloudURL + "/ItemDetail"
        
        var param = [String : Any]()
//        param["bizId"] = user.business!.objectId!
        
        if let business = user.business{
            param["bizId"] = business.objectId!
        }else{
            print("GetItemDetail: business is not set")
            exit(1)
        }
        
        //If menu is not same across all location send locId in parameter
        if !config.isMenuSameForAllLocation{
            if let location = user.location{
                param["locId"] = location.objectId!
            }else{
                print("location is not set")
                exit(1)
            }
            
        }
        
        let requestParams = ["functionName":"getItemDetails", "params" : param] as [String : AnyObject]
        
        print("getItemDetailsForLocation() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error!.localizedDescription)
            }
        }
        
    }
    
    //Returns items of location
    
    /**
     
     @deprecated  use getItemDetails which gets items for a business.
     
    class func getItemDetailsForLocation(_ locId: String, completionHandler: @escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        let urlString = wlCloudURL + "/ItemDetail"
        let requestParams = ["functionName":"getItemDetails", "params" : ["locId": locId]] as Parameters
        
        print("getItemDetailsForLocation() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error!.localizedDescription)
            }
        }
        
    }
 */
    
    // Delete item detail
    class func deleteItemDetail(_ params:[String:Any], completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        let urlString = wlCloudURL + "/ItemDetail"
        
        var requestParams = [String: AnyObject] ()
        
        requestParams["functionName"] = "deleteItemDetail" as AnyObject?
        requestParams["userId"] = userId! as AnyObject?
        requestParams["userSession"] = userSession! as AnyObject?
        requestParams["params"] = params as AnyObject?
        
        print("\ndeleteItemDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
    }
    
    //MARK: /GroupDetail
    
    class func getGroupDetails(completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        let urlString = wlCloudURL + "/GroupDetail"
        
        var param = [String : Any]()
        
//        param["bizId"] = user.business!.objectId!
        
        if let business = user.business{
            param["bizId"] = business.objectId!
        }else{
            print("GetGroupDetail: business is not set")
            exit(1)
        }
        
        //If menu is not same across all location send locId in parameter
        if !config.isMenuSameForAllLocation{
            if let location = user.location{
                param["locId"] = location.objectId!
            }else{
                print("GetGroupDetail: location is not set")
                exit(1)
            }
        }
        let parameter = ["functionName":"getGroupDetails", "params" : param] as [String : AnyObject]
        
        print("getGroupDetailsForLocation() urlString: \(urlString) parameter: \(parameter)")
        
        Alamofire.request(urlString, method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : "TEST-123"]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    // Returns GroupDetails of location
    
    /**
     @deprecated  use getGroupDetails which gets groups for a business.
     
    class func getGroupDetailsForLocation(_ locationId:String,completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        let urlString = wlCloudURL + "/GroupDetail"
        let parameter = ["functionName":"getGroupDetails", "params" : ["locId":locationId]] as [String : Any]
        
        print("getGroupDetailsForLocation() urlString: \(urlString) parameter: \(parameter)")
        
        Alamofire.request(urlString, method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : "TEST-123"]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
 
 */
    
    
    // Creates new group or updates group
    class func updateGroupDetail(_ isNew: Bool, params: [String: Any], completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        let urlString = wlCloudURL + "/GroupDetail"
        
        var requestParams = [String: AnyObject]()
        
        requestParams["functionName"] = (isNew ? "createGroupDetail" : "updateGroupDetail") as AnyObject
        requestParams["userId"] = userId! as AnyObject
        requestParams["userSession"] = userSession! as AnyObject
        requestParams["params"] = params as AnyObject
        
        print("updateGroupDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as? [String:AnyObject], nil)
                }
                
            }
            else{
                
                completionHandler(nil, response.error?.localizedDescription)
            }
        }
    }
    
    // Delete group
    class func deleteGroupDetail(_ params:[String:Any], completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        let urlString = wlCloudURL + "/GroupDetail"
        
        var requestParams = [String: AnyObject] ()
        
        requestParams["functionName"] = "deleteGroupDetail" as AnyObject?
        requestParams["userId"] = userId! as AnyObject?
        requestParams["userSession"] = userSession! as AnyObject?
        requestParams["params"] = params as AnyObject?
        
        print("deleteGroupDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
    }
    
    // create new Location
    class func createLocation(_ param: [String:Any], completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        var requestParams = [String: AnyObject]()
        
        let urlString = wlCloudURL + "/LocationDetail"
        requestParams["userId"] = userId! as AnyObject
        requestParams["userSession"] = userSession! as AnyObject
        requestParams["functionName"] = "createLocationDetail" as AnyObject
        requestParams["params"] = param as AnyObject?
        
        print("createLocationDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as? [String:AnyObject], nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }

    
    
    // Update Location
    class func updateLocation(_ param: [String:Any], completionHandler:@escaping (_ response:[String:AnyObject]?, _ error:String?) -> Void){
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        var requestParams = [String: AnyObject] ()
        
        
        let urlString = wlCloudURL + "/LocationDetail"

        //print("updateLocation: user: " + PFUser.current()!.username! + " session: " + PFUser.current()!.sessionToken! + " urlString: " + urlString)

        requestParams["userId"] = userId! as AnyObject
        requestParams["userSession"] = userSession! as AnyObject
        requestParams["functionName"] = "updateLocationDetail" as AnyObject
        requestParams["params"] = param as AnyObject?
        
        print("updateLocation() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method:.post, parameters: requestParams, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as? [String:AnyObject], nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    // Get details of a Location
    class func getLocationDetail(_ objectId: String, completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
        var requestParams = [String: Any]()
        
        let urlString = wlCloudURL + "/LocationDetail"
        
        requestParams["functionName"] = "getLocationDetail" as AnyObject?
        
        requestParams["params"] = ["objectId":objectId] as AnyObject?
        
        print("getLocationDetail() urlString: \(urlString) requestParams: \(requestParams)")
        
        Alamofire.request(urlString, method: .post, parameters: requestParams,  encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    //MARK: /BusinessDetail
    
    // Returns all locations of business
    class func getAllLocations(_ bizId: String, completionHandler:@escaping (_ response:AnyObject?, _ error:String?) -> Void){
        
       // print("bizId: " + bizId)
        
        let urlString = wlCloudURL + "/BusinessDetail"
        let parameter = ["functionName":"getAllLocations", "params" : ["bizId": bizId]] as [String : AnyObject]
        
        print("getAllLocations() urlString: \(urlString) parameter: \(parameter)")
        
        Alamofire.request(urlString, method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as AnyObject?, nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
        }
        
    }
    
    class func updateBusinessDetail(_ bizId: String, param: [String : Any], completionHandler: @escaping (_ response:[String:Any]?, _ error:String?) -> Void){
        
        //        “functionName” : “updateBusinessDetail”,
        //        “userId” :  “xLGBzCOL1x”,   //user making the request
        //        “userSession” : “user-session-token”,
        //        “params” :
        //        {
        //            "businessId":"business object id",
        //            "updateParams":
        //            {
        //                "columnName":"value"
        //            }
        //        }
        
        if isUserSessionInvalid() {
            completionHandler(nil, ERROR_MSG_INVALID_USER_SESSION)
            return
        }
        
        let urlString = wlCloudURL + "/BusinessDetail"
        
        var parameter = [String: AnyObject] ()
        
        parameter["functionName"] = "updateBusinessDetail" as AnyObject?
        parameter["userId"] = userId! as AnyObject?
        parameter["userSession"] = userSession! as AnyObject?
        parameter["params"]  = param as AnyObject?
        
        print("updateBusinessDetail() urlString: \(urlString) parameter: \(parameter)")
        
        Alamofire.request(urlString, method:.post, parameters: parameter, encoding: JSONEncoding.default, headers: ["X-WL-Web-Key" : webKey]).responseJSON { response in
            
            if response.result.isSuccess {
                
                let result = response.result.value! as! [String: AnyObject]
                
                if let errorMsg = result["error"] as? [String: AnyObject]{
                    
                    completionHandler(nil, errorMsg["message"] as? String)
                }
                else{
                    
                    completionHandler(response.result.value! as? [String: Any], nil)
                }
                
            }
            else{
                completionHandler(nil, response.result.error?.localizedDescription)
            }
            
            
        }
        
    }
    
}

